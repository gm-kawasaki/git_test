(function($) {

	var nav_btn = ".nav_btn";
	var sp_nav = ".sp_nav";

	$(function() {

		var is_mobile =false;
		var is_smp =false;
		var is_retina =false;
		if ( navigator.userAgent.indexOf('iPhone') > 0 || navigator.userAgent.indexOf('iPad') > 0 || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) { 
			is_mobile =true;
			$("body").addClass("mobile");
		}
		if ( navigator.userAgent.indexOf('iPhone') > 0 || navigator.userAgent.indexOf('Android') > 0) { 
			is_smp =true;
			$("body").addClass("smp");
		}
		if ( window.devicePixelRatio >= 2) { 
			is_retina =true;
			$("body").addClass("retina");
		}

		$.GMutil.selflink();
		$.GMutil.nav();
		$.GMutil.rollover();
		$.GMutil.scroll();
		$.GMutil.scrollbreak();
		$.GMutil.popup();
		$.GMutil.rwdImageMaps();
		$.GMutil.sp2x(is_retina);
		
	});

		$.GMutil = {
			selflink: function () {
				$("a").each(function(){
					var urlLink = location.href;
					if(urlLink.substr( urlLink.length-1) ==="/" ){
						urlLink = urlLink+"index.html";
					}
					var tgLink = $(this).prop("href");
					if ( tgLink === urlLink ) {
						$(this).addClass("cr");
					} else if (0 <= urlLink.search(tgLink)) {
						$(this).addClass("cr");
					}
				});	
			},
			nav: function() {
			
				$(nav_btn).click(function() {
					$(sp_nav).stop().slideToggle('slow' );
				});
					
				$(window).resize(function(){
					$(sp_nav).attr("style", "");
				});
				
			},
			//ロールオーバー
			rollover: function() {
				
				$(".fadeimg, .gnavi>li img").each(function() {
					$(this).wrap("<span class='fadeimg_wrap'></span>");
					var This = $(this);
					var Parent = $(this).parent("span.fadeimg_wrap");
					$(this).addClass("off");
					Parent.append(Parent.find("img.off").clone(true).removeClass("off").addClass("on"));
					var onsrc =  Parent.find("img.on").attr("src").replace(new RegExp('(_on)?(\.gif|\.jpg|\.png)$'), "_on$2");
					Parent.find("img.on").attr("src", onsrc);
				});
				
			},
	
			scrollbreak: function() {
				$(window).on("scroll load", function(){
					var scr = $(this).scrollTop();
					
					$("*[data-scrollbreak]").each(function(){
						var scr_break = $(this).data("scrollbreak");
						
						if(String(scr_break).indexOf("%") != -1){
							scr_break = parseInt(scr_break) / 100 * $(window).height();
						}
						
						if(scr > scr_break){
							$(this).addClass("scrolled");
						}else{
							$(this).removeClass("scrolled");
						}
						
					});
					
				});
			},
	
			//ページ内リンクはするするスクロール
			scroll: function() {
				//ドキュメントのスクロールを制御するオブジェクト
				$("a[href^=#]").click(function(){
					var Hash = $(this.hash);
					var HashOffset = $(Hash).offset().top;
					$("html,body").animate({
					scrollTop: HashOffset
					}, 500);
					return false;
				});
			},
			popup: function() {
				$("a.popup").click(function(){
	  					window.open(this.href,'null','scrollbars=yes,resizable=yes,width=750,height=800');
						return false;
				});
				$("a.popup_map").click(function(){
	  					window.open(this.href,'null','scrollbars=yes,resizable=yes,width=750,height=800');
						return false;
				});
			},
			rwdImageMaps: function() {
				$('img[usemap]').rwdImageMaps();
			},
			sp2x: function(is_mobile) {
		
				if (is_mobile) { 
					$("img.2x").each(function() {
						$(this).attr("srcset",$(this).attr("src").replace(new RegExp('(@2x)?(\.gif|\.jpg|\.png)$'), "@2x$2") +" 2x");
					});
				}
					
			}
		};

})(jQuery);



(function($) {
  $.fn.matchWidth = function(breakpoint) {
	  
	if(breakpoint==null || window.matchMedia("(max-width:"+breakpoint+"px)").matches ){
		  var array = [];
		  $(this).each(function(){
			var img = new Image();
			img.src = $(this).attr('src');
			var width = img.width;
			  array.push(width);
		  });

		var arr_max = Math.max.apply(null,array);

		  $(this).each(function(){
			var img = new Image();
			img.src = $(this).attr('src');
			var max = Math.round(img.width / arr_max *10000)/100;
			  $(this).css("max-width",max+"%");
		  });

		}	  
	  
    };
    return this;
})(jQuery);	